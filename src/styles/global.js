import { createGlobalStyle } from 'styled-components';
import BigShouldersText from '../assets/fonts/BigShouldersText-Bold.ttf';
import OpenSansBold from '../assets/fonts/OpenSans-Bold.ttf';
import OpenSansItalic from '../assets/fonts/OpenSans-Italic.ttf';
import OpenSansLight from '../assets/fonts/OpenSans-Light.ttf';
import OpenSans from '../assets/fonts/OpenSans-Regular.ttf';
import OpenSansSemiBold from '../assets/fonts/OpenSans-SemiBold.ttf';
import AvoBold from '../assets/fonts/UTM-AvoBold.ttf';
import FaceBookKT from '../assets/fonts/UTM-FacebookR-K&T.ttf';
import { mixinTextDecorationNone } from './mixin';

const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: 'opensans' ;
    src: url(${OpenSans});
  }

  @font-face {
    font-family: 'opensans-bold' ;
    src: url(${OpenSansBold});
  }

  @font-face {
    font-family: 'opensans-italic' ;
    src: url(${OpenSansItalic});
  }

  @font-face {
    font-family: 'opensans-light' ;
    src: url(${OpenSansLight});
  }

  @font-face {
    font-family: 'opensans-semibold' ;
    src: url(${OpenSansSemiBold});
  }

  @font-face {
    font-family: 'avo-bold' ;
    src: url(${AvoBold});
  }

  @font-face {
    font-family: 'facebook' ;
    src: url(${FaceBookKT});
  }

  @font-face {
    font-family: 'bigshoulders-bold' ;
    src: url(${BigShouldersText});
  }

  body{
    font-family: opensans;
  }

  h1,h2,h3,h4,h5,h6,p{
    margin: 0;
  }

  a{
    &:hover{
      ${mixinTextDecorationNone};
    }
  }

  @media (min-width: 1200px){
    .container, .container-lg, .container-md, .container-sm, .container-xl {
      max-width: 1170px;
    }
  }

  @media (max-width: 991px) {
    section{
    &:nth-child(even){
      background: #f0f6fe;
    }

    &:nth-child(odd){
      background: #fff;
    }
  }
  }
`;

const COLOR = {
  BLACK: {
    HEX: '#000000',
    RGBA: opacity => `rgba(0,0,0,${opacity || 1})`
  }
};

export default GlobalStyles;

export { COLOR };
