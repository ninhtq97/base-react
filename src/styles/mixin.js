import { css } from 'styled-components';

const mixinDisplayFlex = (align, justify, wrap) => `
  display:flex;
  ${align ? `align-items: ${align};` : ''}
  ${justify ? `justify-content: ${justify};` : ''}
  ${wrap ? `flex-wrap: ${wrap}` : ''}
`;

const mixinContentBeforeAfter = css`
  content: '';
`;

const mixinPositionAbsolute = (t, l, r, b, w, h, x = '0', y = '0', i) => `
  position: absolute;
  ${t ? `top: ${t};` : ''}
  ${l ? `left: ${l};` : ''}
  ${r ? `right: ${r};` : ''}
  ${b ? `bottom: ${b};` : ''}
  ${w ? `width: ${w};` : ''}
  ${h ? `height: ${h};` : ''}
  ${x !== '0' || y !== '0' ? `transform: translate(${x},${y});` : ''}
  ${i ? `z-index: ${i};` : ''}
`;

const mixinTextDecorationNone = css`
  text-decoration: none;
`;

const mixinBackgroundImage = (url, size, position, repeat) => `
  ${url ? `background-image: url('${url}');` : ''}
  ${size ? `background-size: ${size};` : ''}
  ${position ? `background-position: ${position};` : ''}
  ${repeat ? `background-repeat: ${repeat};` : ''}
`;

const mixinNavLinkMobile = css`
  padding: 11.5px;
  font-size: 17px;
  display: table;
  margin: 0 auto;
`;

const mixinPaddingMobile = calc => `
  padding-top: ${calc === 'half' ? '20px' : '40px'};
  padding-bottom: ${calc === 'half' ? '20px' : '40px'};
`;

export {
  mixinDisplayFlex,
  mixinContentBeforeAfter,
  mixinPositionAbsolute,
  mixinTextDecorationNone,
  mixinBackgroundImage,
  mixinNavLinkMobile,
  mixinPaddingMobile
};
