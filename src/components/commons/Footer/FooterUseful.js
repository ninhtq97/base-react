import React from 'react';

const FooterUseful = () => {
  const Useful = [
    {
      name: 'About Style',
      path: ''
    },
    {
      name: 'Team',
      path: ''
    },
    {
      name: 'Gallery',
      path: ''
    },
    {
      name: 'News',
      path: ''
    },
    {
      name: 'Contact',
      path: ''
    }
  ];

  return (
    <div className="group-list">
      {Useful.map((e, i) => (
        <div className="list-item" key={i}>
          <a href={e.path}>{e.name}</a>
        </div>
      ))}
    </div>
  );
};

export default FooterUseful;
