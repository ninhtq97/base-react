import React from 'react';
import styled from 'styled-components';
import {
  mixinContentBeforeAfter,
  mixinPositionAbsolute
} from '../../../styles/mixin';

const Styles = styled.div`
  &.first {
    .text {
      font-size: 13px;
      margin-top: 39px;
      margin-left: 3px;
      width: 100%;
      max-width: 300px;
      line-height: 1.6;
    }
  }

  &.column {
    margin-top: 14px;
    &.second {
      margin-left: 104px;
      margin-right: 92px;
    }

    &.third {
      margin-right: 99px;
    }

    &.fourth {
      .group-title {
        margin-bottom: 26px;
      }

      .text {
        max-width: 170px;
        font-size: 13.3px;
        line-height: 1.6;
      }

      .email {
        position: relative;
        margin-top: 12px;
        display: table;

        .email-val {
          background: rgba(255, 255, 255, 0.27);
          border: none;
          border-radius: 2px;
          padding: 4px 75px 6px 14px;
          max-width: 262px;
          color: #fff;

          &::placeholder {
            font-family: 'opensans-italic';
            color: #fff;
            font-size: 13px;
          }
          &:focus {
            outline: none;
          }
        }

        .btn-receive {
          ${mixinPositionAbsolute(
            '50%',
            undefined,
            '3px',
            undefined,
            undefined,
            undefined,
            undefined,
            '-50%',
            '1'
          )}
          padding: 2.5px 14px;
          border-radius: 2px;
          background-color: #1fd392;
          color: #fff;
          font-size: 13px;
        }
      }
    }
  }

  .group-title {
    font-size: 25px;
    font-family: 'opensans-bold';
    margin-bottom: 11px;
  }

  .group-list {
    margin-bottom: 15px;
    .list-item {
      display: table;
      margin-bottom: 9px;

      a {
        position: relative;
        font-size: 13.3px;
        color: #fff;

        &::after {
          ${mixinContentBeforeAfter};
          ${mixinPositionAbsolute(
            undefined,
            '0',
            undefined,
            '-4px',
            '0px',
            '2px',
            undefined,
            undefined,
            undefined
          )};
          background: #fff;
          transition: 0.25s ease;
        }

        &:hover {
          &::after {
            width: 100%;
          }
        }
      }
    }
  }

  @media (min-width: 1200px) {
    &.column {
      &.fourth {
        .email {
          margin-left: -6px;
        }
      }
    }
  }

  @media (max-width: 767px) {
    &.column {
      margin-left: 0 !important;
      margin-right: 0 !important;

      &.fourth {
        .email {
          .email-val {
            width: 100%;
            max-width: unset;
          }
        }
      }
    }
  }

  @media (max-width: 575px) {
    &.first {
      .text {
        margin-top: 20px;
      }
    }

    &.column {
      &.fourth {
        .text {
          max-width: unset;
        }
      }
    }
  }
`;

const FooterGroupItem = ({ classColumn, title, children }) => {
  return (
    <Styles className={`group-item ${classColumn}`}>
      {classColumn !== 'first' && <h4 className="group-title">{title}</h4>}
      {children}
    </Styles>
  );
};

export default FooterGroupItem;
