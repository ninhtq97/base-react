import React from 'react';

const FooterServices = () => {
  const Services = [
    {
      name: 'SEO for Small Business',
      path: ''
    },
    {
      name: 'SEO for Local Services',
      path: ''
    },
    {
      name: 'Enterprise SEO',
      path: ''
    },
    {
      name: 'National SEO',
      path: ''
    },
    {
      name: 'International SEO',
      path: ''
    }
  ];

  return (
    <div className="group-list">
      {Services.map((e, i) => (
        <div className="list-item" key={i}>
          <a href={e.path}>{e.name}</a>
        </div>
      ))}
    </div>
  );
};

export default FooterServices;
