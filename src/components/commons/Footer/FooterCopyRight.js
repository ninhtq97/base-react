import React from 'react';
import styled from 'styled-components';

const Styles = styled.div`
  padding: 8.5px;
  text-align: center;
  font-family: 'opensans-italic';
  font-size: 13px;
  word-spacing: 0.7px;
  background: rgba(255, 255, 255, 0.18);
`;

const FooterCopyRight = ({ className }) => {
  return (
    <Styles className={`${className}-copyright`}>
      Copyright © 2020 TIẾP THỊ 2 CẤP. All Rights Reserved.
    </Styles>
  );
};

export default FooterCopyRight;
