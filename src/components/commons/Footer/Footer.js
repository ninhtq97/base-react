import React from 'react';
import { Container } from 'reactstrap';
import styled from 'styled-components';
import BackgroundFooter from '../../../assets/images/bg-footer.jpg';
import {
  mixinBackgroundImage,
  mixinContentBeforeAfter,
  mixinDisplayFlex,
  mixinPaddingMobile,
  mixinPositionAbsolute
} from '../../../styles/mixin';
import FooterCopyRight from './FooterCopyRight';
import FooterFirstColumn from './FooterFirstColumn';
import FooterFollow from './FooterFollow';
import FooterGroupItem from './FooterGroupColumn';
import FooterServices from './FooterServices';
import FooterUseful from './FooterUseful';

const className = 'footer';

const Styles = styled.footer`
  position: relative;
  color: #fff;

  &::before {
    ${mixinContentBeforeAfter};
    ${mixinPositionAbsolute(
      '0',
      '0',
      undefined,
      undefined,
      '100%',
      '100%',
      undefined,
      undefined,
      '-1'
    )};
    ${mixinBackgroundImage(BackgroundFooter, 'cover', '0 0', 'no-repeat')};
  }

  .${className}-cover {
    padding-top: 57px;
    padding-bottom: 15px;

    .${className}-list {
      ${mixinDisplayFlex()};
      margin-bottom: 22px;
      .group {
        ${mixinDisplayFlex(undefined, 'space-between', 'wrap')};
      }
    }
  }

  @media (max-width: 991px) {
    .${className}-cover {
      ${mixinPaddingMobile};
    }
  }

  @media (max-width: 767px) {
    .${className}-cover {
      .${className}-list {
        display: table;
        margin-left: auto;
        margin-right: auto;
        .group {
          flex-direction: column;
        }
      }
    }
  }

  @media (max-width: 575px) {
    .${className}-cover {
      ${mixinPaddingMobile('half')};
    }
  }
`;

const Footer = () => {
  return (
    <Styles>
      <Container>
        <div className={`${className}-cover`}>
          <div className={`${className}-list`}>
            <div className={`group`}>
              <FooterGroupItem classColumn="first">
                <FooterFirstColumn className={className} />
              </FooterGroupItem>
              <FooterGroupItem classColumn="column second" title="Useful Link">
                <FooterUseful />
              </FooterGroupItem>
              <FooterGroupItem classColumn="column third" title="Services">
                <FooterServices />
              </FooterGroupItem>
              <FooterGroupItem classColumn="column fourth" title="Theo dõi">
                <FooterFollow />
              </FooterGroupItem>
            </div>
          </div>
          <FooterCopyRight className={className} />
        </div>
      </Container>
    </Styles>
  );
};

export default Footer;
