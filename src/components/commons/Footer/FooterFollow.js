import React from 'react';

const FooterFollow = () => {
  return (
    <>
      <p className="text">
        Theo dõi bản tin của chúng tôi để được cập nhật về cơ quan.
      </p>
      <div className="email">
        <input type="text" className="email-val" placeholder="Email của bạn" />
        <button type="button" className="btn btn-receive">
          Nhận
        </button>
      </div>
    </>
  );
};

export default FooterFollow;
