import {
  faFacebookF,
  faTwitter,
  faYoutube
} from '@fortawesome/free-brands-svg-icons';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styled from 'styled-components';
import {
  mixinContentBeforeAfter,
  mixinDisplayFlex,
  mixinPositionAbsolute
} from '../../../styles/mixin';

const Styles = styled.div`
  display: flex;
  margin-top: 27px;

  .social-item {
    margin-right: 10px;

    &:last-child {
      margin-right: 0;
    }

    a {
      ${mixinDisplayFlex('center', 'center')}
      position: relative;
      color: #fff;
      text-align: center;
      width: 40px;
      height: 40px;
      border-radius: 50%;
      background-color: rgba(255, 255, 255, 0.28);

      &:hover {
        &::after {
          transform: scale(1.4);
          opacity: 0;
        }
      }

      &::after {
        ${mixinContentBeforeAfter}
        ${mixinPositionAbsolute(
          '0',
          '0',
          undefined,
          undefined,
          '100%',
          '100%',
          undefined,
          undefined,
          '-1'
        )};
        background-color: rgba(255, 255, 255, 0.28);
        border-radius: 50%;
        transition: 0.4s linear;
      }
    }
  }
`;

const FooterSocial = () => {
  const Socials = [
    {
      icon: faPaperPlane
    },
    {
      icon: faTwitter
    },
    {
      icon: faYoutube
    },
    {
      icon: faFacebookF
    }
  ];

  return (
    <Styles className="social">
      {Socials.map((e, i) => (
        <div className="social-item" key={i}>
          <a href="/" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={e.icon}></FontAwesomeIcon>
          </a>
        </div>
      ))}
    </Styles>
  );
};

export default FooterSocial;
