import { faEllipsisV, faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import styled from 'styled-components';
import { mixinPositionAbsolute } from '../../../styles/mixin';

const Styles = styled.div`
  position: relative;
  z-index: 1;
  width: 25px;
  height: 33px;
  text-align: center;

  .bar-item {
    color: #fff;
    font-size: 22px;
    ${mixinPositionAbsolute(
      '50%',
      '50%',
      undefined,
      undefined,
      '100%',
      '100%',
      '-50%',
      '-50%',
      '-1'
    )};
    transition: all 0.35s ease;

    &.bar-icon__open {
      z-index: 1;
    }
    &.bar-icon__close {
      opacity: 0;
    }
  }

  @media (max-width: 767px) {
    &.open {
      .bar-icon__open {
        opacity: 0;
        z-index: -1;
      }

      .bar-icon__close {
        opacity: 1;
        z-index: 1;
      }
    }
  }
`;

const HeaderBar = ({ className, documentWidth, isOpenNav, openNavHandle }) => {
  return (
    <>
      {documentWidth < 768 && (
        <Styles className={`${className}-bar ${isOpenNav ? 'open' : 'hide'}`}>
          <div className="bar-item bar-icon__open" onClick={openNavHandle}>
            <FontAwesomeIcon icon={faEllipsisV}></FontAwesomeIcon>
            <FontAwesomeIcon icon={faEllipsisV}></FontAwesomeIcon>
            <FontAwesomeIcon icon={faEllipsisV}></FontAwesomeIcon>
          </div>
          <div className="bar-item bar-icon__close" onClick={openNavHandle}>
            <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
          </div>
        </Styles>
      )}
    </>
  );
};

export default HeaderBar;
