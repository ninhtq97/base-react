import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { NAME_PAGES, ROUTES } from '../../../routes';
import {
  mixinDisplayFlex,
  mixinNavLinkMobile,
  mixinTextDecorationNone
} from '../../../styles/mixin';

const Styles = styled.div`
  background: #1fd392;
  border-radius: 50px;
  padding: 1px;
  margin-top: 34px;
  margin-left: 10px;
  margin-right: 4px;

  .authen-cover {
    ${mixinDisplayFlex('', '', 'wrap')};
    border-radius: 50px;
    overflow: hidden;
    font-size: 12px;
  }

  .authen-item {
    .authen-link {
      color: #fff;
      font-size: 1em;
      padding-top: 3px;
      padding-bottom: 3px;
      display: block;

      &.register {
        padding-right: 17px;
        padding-left: 10px;
      }

      &.sign-in {
        padding-right: 6px;
        padding-left: 3px;
      }
    }
  }

  @media (min-width: 768px) {
    .authen-cover {
      .authen-item {
        .authen-link {
          &:hover {
            color: #1fd392;
            background: #fff;
            ${mixinTextDecorationNone}
          }
        }
      }
    }
  }

  @media (max-width: 1199px) {
    margin-top: 0;
  }

  @media (max-width: 767px) {
    background: transparent;
    flex-basis: 50%;
    margin: 0;
    .authen-cover {
      display: block;
      border-radius: 0;
      overflow: unset;

      .authen-item {
        &:not(:last-child) {
          margin-bottom: 10px;
        }
        .authen-link {
          ${mixinNavLinkMobile};
          width: 212px;
          text-align: center;
          font-weight: bold;
          text-transform: uppercase;
          border-radius: 3px;
          position: relative;
          z-index: 0;
          overflow: hidden;

          &::after {
            background: #fff;
            content: '';
            height: 155px;
            left: -75px;
            opacity: 0.5;
            position: absolute;
            top: -50px;
            transform: rotate(35deg);
            transition: all 550ms cubic-bezier(0, 0, 0.51, 1.18);
            width: 50px;
            z-index: -10;
          }

          &:hover {
            &:after {
              left: 120%;
            }
          }

          &.register {
            background: #59b3f7;
          }

          &.sign-in {
            background: #1fd392;
          }
        }
      }
    }
  }

  @media (max-width: 575px) {
    margin-top: 20px;
    margin-bottom: 20px;
  }
`;

const HeaderAuthen = ({ className }) => {
  const Authentication = [
    {
      name: NAME_PAGES.REGISTER,
      path: ROUTES.AUTH.REGISTER.PATH,
      class: 'register'
    },
    {
      name: NAME_PAGES.LOGIN,
      path: ROUTES.AUTH.LOGIN.PATH,
      class: 'sign-in'
    }
  ];

  return (
    <Styles className={`${className}-authen`}>
      <div className="authen-cover">
        {Authentication.map((e, i) => (
          <div className="authen-item" key={i}>
            <Link className={`authen-link ${e.class}`} to={e.path} key={i}>
              {e.name}
            </Link>
          </div>
        ))}
      </div>
    </Styles>
  );
};

export default HeaderAuthen;
