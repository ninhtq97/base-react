import React, { useContext, useState } from 'react';
import { Container } from 'reactstrap';
import styled from 'styled-components';
import { DocumentWidthContext } from '../../../context';
import { mixinDisplayFlex } from '../../../styles/mixin';
import Logo from '../../private/external/Logo';
import HeaderAuthen from './HeaderAuthen';
import HeaderBar from './HeaderBar';
import HeaderNav from './HeaderNav';

const className = 'header';

const Styles = styled.header`
  background: rgba(0, 0, 0, 0.57);
  border-bottom: 1px solid #20d393;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 100;

  .${className}-cover {
    ${mixinDisplayFlex('', 'space-between', 'wrap')};

    .${className}-logo {
      font-size: 17px;
      position: relative;
      z-index: 1;
      a {
        display: block;

        img {
          width: 17.12em;
          max-width: 291px;
          transition: 0.25s ease;
        }
      }
    }
  }

  @media (min-width: 992px) {
    .${className}-cover {
      .${className}-logo {
        a {
          margin-top: 1em;
        }
      }
    }
  }

  @media (min-width: 576px) {
    .${className}-cover {
      .${className}-group {
        ${mixinDisplayFlex('flex-start', 'flex-end', 'wrap')};
      }
    }
  }

  @media (max-width: 1199px) {
    .${className}-cover {
      .${className}-group {
        align-items: center;
      }
    }
  }

  @media (max-width: 991px) {
    .${className}-cover {
      .${className}-logo {
        font-size: 10px;
        ${mixinDisplayFlex('center')};
      }
    }
  }

  @media (max-width: 767px) {
    overflow: hidden;
    transition: all 0.35s ease;

    &.open {
      background: #1f3552;
      overflow: unset;

      .${className}-cover {
        .${className}-group {
          opacity: 1;
          transform: translate(0, 0);
          pointer-events: auto;
        }
      }
    }

    .${className}-cover {
      padding: 10px 0;
      align-items: center;

      .${className}-logo {
        font-size: 12px;
      }

      .${className}-group {
        position: fixed;
        top: 0;
        left: 0;
        background: #1f3552;
        width: 100%;
        height: calc(100% - 63px);
        align-items: flex-start;
        justify-content: space-around;
        margin-top: 63px;
        padding-top: 40px;
        pointer-events: none;
        opacity: 0;
        transform: translate(0, -16px);
        transition: all 0.35s ease;
        overflow: auto;
      }
    }
  }
`;

const Header = () => {
  // Context
  const documentWidth = useContext(DocumentWidthContext);

  // State
  const [isOpenNav, setIsOpenNav] = useState(false);

  // Function Handle
  const openNavHandle = () => {
    setIsOpenNav(!isOpenNav);
  };

  return (
    <Styles id={className} className={`${isOpenNav ? 'open' : 'hide'}`}>
      <Container>
        <div className={`${className}-cover`}>
          <Logo className={className} />
          <HeaderBar
            className={className}
            documentWidth={documentWidth}
            isOpenNav={isOpenNav}
            openNavHandle={openNavHandle}
          ></HeaderBar>
          <div className={`${className}-group`}>
            <HeaderNav
              className={className}
              documentWidth={documentWidth}
            ></HeaderNav>
            <HeaderAuthen className={className}></HeaderAuthen>
          </div>
        </div>
      </Container>
    </Styles>
  );
};

export default Header;
