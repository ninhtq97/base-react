import React from 'react';
import { NavLink } from 'react-router-dom';
import styled, { keyframes } from 'styled-components';
import { navRoutes } from '../../../routes';
import { mixinDisplayFlex, mixinNavLinkMobile } from '../../../styles/mixin';

const fill = keyframes`
  0% {
    height: 0;
  }
  100% {
    height: 50px;
    background: linear-gradient(to bottom,rgba(31, 31, 31, 0),rgba(32, 209, 146, 0.45));
  }
`;

const Styles = styled.div`
  font-size: 15px;

  .nav-item {
    .nav-link {
      display: block;
      color: #fff;
      text-transform: uppercase;
      position: relative;
      padding: 2.3em 1.35em 2.6em;
      font-size: 1em;

      svg {
        height: 45px;
        left: 0;
        position: absolute;
        top: 0;
        width: 100%;
      }

      rect {
        fill: none;
        stroke: #fff;
        stroke-width: 2;
        stroke-dasharray: 422, 0;
        transition: all 0.35s linear;
      }
    }
  }

  @media (min-width: 768px) {
    .nav-item {
      .nav-link {
        transition: all 1s;
        &::after {
          content: '';
          position: absolute;
          bottom: 0;
          left: 0;
          width: 100%;
          height: 0;
          background: linear-gradient(
            to bottom,
            rgba(31, 31, 31, 0),
            rgba(32, 209, 146, 0.45)
          );
          z-index: -1;
        }

        &:hover,
        &.current {
          color: #1fd392;
          font-weight: bold;

          &::after {
            animation: ${fill} 0.5s forwards;
          }
        }
      }
    }
  }
  @media (min-width: 576px) {
    ${mixinDisplayFlex('center', 'flex-end', 'wrap')};
  }

  @media (max-width: 1199px) {
    font-size: 14px;
    .nav-item {
      .nav-link {
        padding-left: 1em;
        padding-right: 1em;
      }
    }
  }

  @media (max-width: 991px) {
    font-size: 12px;
    .nav-item {
      .nav-link {
        padding-top: 2em;
        padding-bottom: 2em;
      }
    }
  }

  @media (max-width: 767px) {
    display: block;
    font-size: 15px;
    flex-basis: 50%;

    .nav-item {
      &:not(:last-child) {
        margin-bottom: 10px;
      }

      .nav-link {
        ${mixinNavLinkMobile};
        min-width: 135px;

        &:hover {
          font-weight: 900;
          letter-spacing: 1px;

          rect {
            stroke-width: 5;
            stroke-dasharray: 15, 310;
            stroke-dashoffset: 48;
            transition: all 1.35s cubic-bezier(0.19, 1, 0.22, 1);
          }
        }

        span {
          transition: all 0.6s ease;
        }
      }
    }
  }
`;

const HeaderNav = ({ className, documentWidth }) => {
  return (
    <Styles className={`${className}-nav`}>
      {navRoutes.map((e, i) => (
        <div className="nav-item" key={i}>
          <NavLink
            to={e.path}
            exact
            className="nav-link"
            activeClassName="current"
          >
            {documentWidth < 768 && (
              <svg>
                <rect x="0" y="0" fill="none" width="100%" height="100%" />
              </svg>
            )}
            <span>{e.name}</span>
          </NavLink>
        </div>
      ))}
    </Styles>
  );
};

export default HeaderNav;
