import styled from 'styled-components';
import {
  mixinContentBeforeAfter,
  mixinPositionAbsolute
} from '../../styles/mixin';

const Title = styled.h4`
  display: table;
  padding: 26px 41px 25px 44px;
  font-family: 'avo-bold';
  font-size: 2.34rem;
  text-transform: uppercase;
  color: #65719d;
  position: relative;
  ${props => (props.center ? 'margin-left: auto; margin-right:auto' : '')};

  @media (min-width: 576px) {
    &::before,
    &::after {
      ${mixinContentBeforeAfter};
      ${mixinPositionAbsolute(
        undefined,
        undefined,
        undefined,
        undefined,
        '100%',
        '82px',
        undefined,
        undefined,
        undefined
      )};
    }

    &::before {
      top: 0;
      left: 0;
      background: ${props =>
        props.borderColor === 'half' ? '#65719d' : '#1fd392'};
      clip-path: polygon(
        72.5% 0%,
        0% 0%,
        0% 100%,
        22.5% 100%,
        22.5% 98%,
        0.4% 98%,
        0.4% 2.5%,
        72.5% 2.5%
      );
    }
    &::after {
      bottom: 0;
      left: 0;
      background: #1fd392;
      clip-path: polygon(
        77.5% 0%,
        100% 0%,
        100% 100%,
        27.5% 100%,
        27.5% 98%,
        99.7% 98%,
        99.7% 2.5%,
        77.5% 2.5%
      );
    }
  }

  @media (max-width: 767px) {
    font-size: 2rem;
  }

  @media (max-width: 575px) {
    font-size: 1.4rem;
    padding: 0;
  }
`;

export default Title;
