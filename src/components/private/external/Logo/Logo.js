import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../../assets/images/logo.png';

const Logo = ({ className }) => {
  return (
    <div className={`${className}-logo`}>
      <Link to="/">
        <img className="logo-img" src={logo} alt="Logo Header" />
      </Link>
    </div>
  );
};

export default Logo;
