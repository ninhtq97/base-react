import React from 'react';
import { Redirect } from 'react-router-dom';

function PrivateRoute({ children }) {
  const getToken = localStorage.getItem('token');

  if (getToken) {
    return children;
  }
  return <Redirect to="/login" />;
}

function BackToHome({ children }) {
  const getToken = localStorage.getItem('token');

  if (getToken) {
    return <Redirect to="/dashboad" />;
  }

  return children;
}

export default PrivateRoute;

export { BackToHome };
