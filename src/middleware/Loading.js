import PropTypes from 'prop-types';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { UserTokenContext } from '../App';
import { getUser } from '../redux/auth/authActions';
import { fetchWallet } from '../redux/wallet/walletActions';
import { STATUS_CODE } from '../utils/variable';

function Loading({ history, children, getUser, fetchWallet }) {
  const [isLoading, setIsLoading] = useState(true);
  const { userToken } = useContext(UserTokenContext);

  const getData = useCallback(async () => {
    const user = await getUser(userToken);

    const wallet = await fetchWallet(userToken);

    if (
      user &&
      user.status === STATUS_CODE.SUCCESS &&
      wallet &&
      wallet.status === STATUS_CODE.SUCCESS
    ) {
      setIsLoading(false);
    }
    if (
      (user && user.status === STATUS_CODE.UNAUTHORIZED) ||
      (wallet && wallet.status === STATUS_CODE.UNAUTHORIZED)
    ) {
      localStorage.removeItem('token');
      history.push('/login');
    }
    if (
      (user && user.status === STATUS_CODE.SERVER_ERROR) ||
      (wallet && wallet.status === STATUS_CODE.SERVER_ERROR)
    ) {
      history.push('/error/500');
    }
  }, [fetchWallet, getUser, history, userToken]);

  useEffect(() => {
    getData();
  }, [getData]);

  if (isLoading) {
    return (
      <div className="loading">
        <div id="loader">
          <div id="shadow"></div>
          <div id="box"></div>
        </div>
      </div>
    );
  }

  return children;
}

Loading.propTypes = {
  getUser: PropTypes.func.isRequired,
  fetchWallet: PropTypes.func.isRequired
};

export default connect(null, { getUser, fetchWallet })(Loading);
