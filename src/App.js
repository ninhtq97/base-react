import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { DocumentWidthProvider } from './context';
import { authenRoutes, navRoutes, subPageRoutes } from './routes';

function App() {
  const [documentWidth, setDocumentWidth] = useState(window.innerWidth);

  const resizeWidthHandle = () => setDocumentWidth(window.innerWidth);

  useEffect(() => {
    window.addEventListener('resize', resizeWidthHandle);
    return () => {
      window.removeEventListener('resize', resizeWidthHandle);
    };
  }, []);

  const getRoutes = routes => {
    return routes.map((prop, key) => (
      <Route exact path={prop.path} component={prop.component} key={key} />
    ));
  };

  return (
    <DocumentWidthProvider value={documentWidth}>
      <Router>
        <Switch>
          {getRoutes(navRoutes)}
          {getRoutes(authenRoutes)}
          {getRoutes(subPageRoutes)}
        </Switch>
      </Router>
    </DocumentWidthProvider>
  );
}

export default App;
