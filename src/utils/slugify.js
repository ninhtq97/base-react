import slugify from 'slugify';

const parseToSlug = string => {
  return slugify(string, { remove: /[.]/, lower: true });
};

export default parseToSlug;
