import React from 'react';
import styled from 'styled-components';
import Footer from '../components/commons/Footer/Footer';
import Header from '../components/commons/Header/Header';
import GlobalStyles from '../styles/global';

const MainWrapper = styled.main`
  padding-top: 100px;
`;

const External = ({ children }) => {
  return (
    <>
      <GlobalStyles />
      <Header />
      <MainWrapper>{children}</MainWrapper>
      <Footer />
    </>
  );
};

export default External;
