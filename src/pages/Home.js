import React from 'react';
import AboutUs from '../components/private/external/Home/AboutUs';
import Banner from '../components/private/external/Home/Banner';
import External from '../layouts/External';

const Home = props => {
  // Context

  return (
    <External>
      <Banner />
      <AboutUs />
    </External>
  );
};

export default Home;
