import React from 'react';
import External from '../layouts/External';

const Guide = () => {
  return (
    <External>
      <div>Guide</div>
    </External>
  );
};

export default Guide;
