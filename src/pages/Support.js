import React from 'react';
import External from '../layouts/External';

const Support = () => {
  return (
    <External>
      <div>Support</div>
    </External>
  );
};

export default Support;
