import React from 'react';
import { useParams } from 'react-router-dom';

const EnterpriseHighlights = () => {
  const { slug } = useParams();

  return <div>Enterprise Highlights {slug}</div>;
};

export default EnterpriseHighlights;
