import React from 'react';
import { useParams } from 'react-router-dom';

const Details = () => {
  const { slug } = useParams();

  return <div>Details {slug}</div>;
};

export default Details;
