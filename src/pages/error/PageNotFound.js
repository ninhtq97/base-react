import React from 'react';
import { Link } from 'react-router-dom';
import { NAME_PAGES } from '../../routes';

function PageNotFound() {
  return (
    <div className="error error-404">
      <h1>{NAME_PAGES.PAGE_NOT_FOUND}</h1>
      <img src={''} alt="" />
      <Link to="/" className="back-home">
        Back to home
      </Link>
    </div>
  );
}

export default PageNotFound;
