import React from 'react';
import { Link } from 'react-router-dom';
import { NAME_PAGES } from '../../routes';

function InternalServerError() {
  return (
    <div className="error error-500">
      <h1>{NAME_PAGES.PAGE_ERROR_500}</h1>
      <img src={''} alt="" />
      <Link to="/" className="back-home">
        Back to home
      </Link>
    </div>
  );
}

export default InternalServerError;
