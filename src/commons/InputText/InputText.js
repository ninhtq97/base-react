import styled from 'styled-components';

const InputText = styled.input`
  border: 1px solid #eee;
  border-radius: 2px;
`;

export default InputText;
