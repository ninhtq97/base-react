import styled from 'styled-components';

const Button = styled.button`
  border: none;
  display: block;
`;

export default Button;
