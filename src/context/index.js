import { createContext } from 'react';

const DocumentWidthContext = createContext();
const DocumentWidthProvider = DocumentWidthContext.Provider;

export { DocumentWidthContext, DocumentWidthProvider };
