import Login from '../pages/auth/Login';
import Register from '../pages/auth/Register';
import InternalServerError from '../pages/error/InternalServerError';
import PageNotFound from '../pages/error/PageNotFound';
import Guide from '../pages/Guide';
import Home from '../pages/Home';
import Introduct from '../pages/Introduct';
import Contact from '../pages/subpage/Contact';
import Details from '../pages/subpage/Details';
import EnterpriseHighlights from '../pages/subpage/EnterpriseHighlights';
import NewsAndEvents from '../pages/subpage/NewsAndEvents';
import Support from '../pages/Support';
import parseToSlug from '../utils/slugify';

const NAME_PAGES = {
  INDEX: 'Trang chủ',
  INTRODUCT: 'Giới thiệu',
  GUIDE: 'Hướng dẫn',
  SUPPORT: 'Hỗ trợ',
  REGISTER: 'Đăng ký',
  LOGIN: 'Đăng nhập',
  CONTACT: 'Liên Hệ',
  DETAILS: 'Chi tiết',
  ENTERPRISE: 'Doanh nghiệp nổi bật',
  NEWS_EVENTS: 'Tin tức - Sự kiện',
  PAGE_ERROR_500: '505 - Internal Server Error ',
  PAGE_NOT_FOUND: '404 - Page Not Found'
};

const ROUTES = {
  NAV: {
    INDEX: {
      PATH: '/',
      COMPONENT: Home
    },
    INTRODUCT: {
      PATH: `/${parseToSlug(NAME_PAGES.INTRODUCT)}`,
      COMPONENT: Introduct
    },
    GUIDE: {
      PATH: `/${parseToSlug(NAME_PAGES.GUIDE)}`,
      COMPONENT: Guide
    },
    SUPPORT: {
      PATH: `/${parseToSlug(NAME_PAGES.SUPPORT)}`,
      COMPONENT: Support
    }
  },
  AUTH: {
    REGISTER: {
      PATH: `/${parseToSlug(NAME_PAGES.REGISTER)}`,
      COMPONENT: Register
    },
    LOGIN: {
      PATH: `/${parseToSlug(NAME_PAGES.LOGIN)}`,
      COMPONENT: Login
    }
  },
  SUB_PAGE: {
    CONTACT: {
      PATH: `/${parseToSlug(NAME_PAGES.CONTACT)}`,
      COMPONENT: Contact
    },
    DETAILS: {
      PATH: `/${parseToSlug(NAME_PAGES.DETAILS)}`,
      COMPONENT: Details
    },
    ENTERPRISE: {
      PATH: '/doanh-nghiep', // OR PATH: `/${parseToSlug(NAME_PAGES.ENTERPRISE)}`
      COMPONENT: EnterpriseHighlights
    },
    NEWS_EVENTS: {
      PATH: `/${parseToSlug(NAME_PAGES.NEWS_EVENTS)}`,
      COMPONENT: NewsAndEvents
    },
    PAGE_ERROR_500: {
      PATH: '/error/500',
      COMPONENT: InternalServerError
    },
    PAGE_NOT_FOUND: {
      PATH: '*',
      COMPONENT: PageNotFound
    }
  }
};

const navRoutes = [
  {
    path: ROUTES.NAV.INDEX.PATH,
    name: NAME_PAGES.INDEX,
    component: ROUTES.NAV.INDEX.COMPONENT
  },
  {
    path: ROUTES.NAV.INTRODUCT.PATH,
    name: NAME_PAGES.INTRODUCT,
    component: ROUTES.NAV.INTRODUCT.COMPONENT
  },
  {
    path: ROUTES.NAV.GUIDE.PATH,
    name: NAME_PAGES.GUIDE,
    component: ROUTES.NAV.GUIDE.COMPONENT
  },
  {
    path: ROUTES.NAV.SUPPORT.PATH,
    name: NAME_PAGES.SUPPORT,
    component: ROUTES.NAV.SUPPORT.COMPONENT
  }
];

const authenRoutes = [
  {
    path: ROUTES.AUTH.REGISTER.PATH,
    name: NAME_PAGES.REGISTER,
    component: ROUTES.AUTH.REGISTER.COMPONENT
  },
  {
    path: ROUTES.AUTH.LOGIN.PATH,
    name: NAME_PAGES.LOGIN,
    component: ROUTES.AUTH.LOGIN.COMPONENT
  }
];

const subPageRoutes = [
  {
    path: ROUTES.SUB_PAGE.CONTACT.PATH,
    name: NAME_PAGES.CONTACT,
    component: ROUTES.SUB_PAGE.CONTACT.COMPONENT
  },
  {
    path: ROUTES.SUB_PAGE.DETAILS.PATH + '/:slug',
    name: NAME_PAGES.DETAILS,
    component: ROUTES.SUB_PAGE.DETAILS.COMPONENT
  },
  {
    path: ROUTES.SUB_PAGE.ENTERPRISE.PATH + '/:slug',
    name: NAME_PAGES.ENTERPRISE,
    component: ROUTES.SUB_PAGE.ENTERPRISE.COMPONENT
  },
  {
    path: ROUTES.SUB_PAGE.NEWS_EVENTS.PATH,
    name: NAME_PAGES.NEWS_EVENTS,
    component: ROUTES.SUB_PAGE.NEWS_EVENTS.COMPONENT
  },
  {
    path: ROUTES.SUB_PAGE.NEWS_EVENTS.PATH,
    name: NAME_PAGES.NEWS_EVENTS,
    component: ROUTES.SUB_PAGE.NEWS_EVENTS.COMPONENT
  },
  {
    path: ROUTES.SUB_PAGE.PAGE_ERROR_500.PATH,
    name: NAME_PAGES.PAGE_ERROR_500,
    component: ROUTES.SUB_PAGE.PAGE_ERROR_500.COMPONENT
  },
  {
    path: ROUTES.SUB_PAGE.PAGE_NOT_FOUND.PATH,
    name: NAME_PAGES.PAGE_NOT_FOUND,
    component: ROUTES.SUB_PAGE.PAGE_NOT_FOUND.COMPONENT
  }
];

export { NAME_PAGES, ROUTES, navRoutes, authenRoutes, subPageRoutes };
